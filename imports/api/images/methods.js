import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';


import { Images } from './images.js';


export const findBackground =  new ValidatedMethod({
    name: 'images.findbackground',
    validate: new SimpleSchema({
        ids: {type: String},
    }).validator(),
    run({ids}){

        return Images.findOne({'metadata.productId' : ids});
    }
})

export const getAllImages =  new ValidatedMethod({
    name: 'images.getallimages',
    validate: new SimpleSchema({
        ids: {type: String},
    }).validator(),
    run({ids}){

        return Images.find({'metadata.productId' : ids});
    }
})


const IMAGES_METHODS = _.pluck([


],'name');

if (Meteor.isServer) {
    // Only allow 5 todos operations per connection per second
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(IMAGES_METHODS, name);
        },

        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}
