import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { EasySearch } from 'meteor/easy:search';


export const Categories = new Mongo.Collection('Categories');
Categories.deny({
  insert(){
    return false;
  },
  update(){
    return false;
  },
  remove(){
    return false;
  }
});


Categories.schema = new SimpleSchema({
  name : {
    type: String
  },
  color: {
    type: String,
    optional: true
  },
  parentId:{
    type: String,
    optional: true
  }
})

Categories.attachSchema(Categories.schema);


Categories.publicFields = {
  name: 1,
  color: 1,
  parentId: 1
};
