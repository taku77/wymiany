import { Mongo } from 'meteor/mongo';


import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const createSquareThumb = (fileObj, readStream, writeStream) => {
  var size = '150';
  gm(readStream).autoOrient().resize(size, size + '^').gravity('Center').extent(size, size).stream('PNG').pipe(writeStream);
};


export const Images = new FS.Collection("images", {
  stores: [
      new FS.Store.FileSystem("images", {path: process.env.PWD+"/upload/images"}),
       new FS.Store.FileSystem("thumbs", {transformWrite: createSquareThumb, path: process.env.PWD+"/upload/thumbs"})
      ]
});


Images.allow({
  'insert': function () {
    return true;
  }
});