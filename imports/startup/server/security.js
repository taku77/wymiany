import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { Categories } from '../../api/categories/categories.js';
FS.debug =true;
// Don't let people write arbitrary data to their 'profile' field from the client
Meteor.users.deny({
  update() {
    return true;
  },
});

// Get a list of all accounts methods by running `Meteor.server.method_handlers` in meteor shell
const AUTH_METHODS = [
  'login',
  'logout',
  'logoutOtherClients',
  'getNewToken',
  'removeOtherTokens',
  'configureLoginService',
  'changePassword',
  'forgotPassword',
  'resetPassword',
  'verifyEmail',
  'createUser',
  'ATRemoveService',
  'ATCreateUserServer',
  'ATResendVerificationEmail',
];
Meteor.startup(function(){
    const categories = Categories.find({}).count();

    const catNames = [
      'Antyki i Sztuka',
      'Bilety',
      'Biuro i Reklama',
      'Biżuteria i Zegarki',
      'Delikatesy',
      'Dla Dzieci',
      'Dom i Ogród',
      'Erotyka',
      'Filmy',
      'Fotografia',
      'Gry',
      'Instrumenty',
      'Komputery',
    ]
    if(categories == 0)
    {
        catNames.forEach((k,v)=>{

        })
    }
});
if (Meteor.isServer) {
  // Only allow 2 login attempts per connection per 5 seconds
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(AUTH_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
  }, 2, 5000);
}
