import './product-show.html';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Products } from '../../../api/products/products.js';
import {
    findBackground,
    getAllImages
} from '../../../api/images/methods.js';


Template.Product_show.helpers({

  product(){
    return Products.findOne({_id:FlowRouter.getParam('_id')});
  },
  images(){
        return getAllImages.call({
            ids : FlowRouter.getParam('_id')
        }).map(function(picture, index) {
      if (index === 0)
        picture.isFirst = true;

      return picture;
    });;
  },
  userProduct(){
    const userId = Products.findOne({_id:FlowRouter.getParam('_id')}).userId;
    return Meteor.users.findOne(userId);
  }
});
