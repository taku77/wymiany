import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { AccountsTemplates } from 'meteor/useraccounts:core';


import '../../ui/layouts/app-body.js';
import '../../ui/pages/user-profile.js';
import '../../ui/pages/product-box.js';
import '../../ui/pages/user-profile-list.js';
import '../../ui/pages/product-show/product-show.js';
import '../../ui/pages/product-list/product-list.js';
import '../../ui/pages/categories-insert/categories-insert.js';
import '../../ui/accounts/accounts-templates.js';

FlowRouter.route('/user/:_id',{
    name: 'User.show',
    action(){
        BlazeLayout.render('App_body', { main: 'User_profile'})
    },
});

FlowRouter.route('/', {
    name: 'App.home',
    action() {
        BlazeLayout.render('App_body');
    },
});


FlowRouter.route('/listusers', {
    name: 'Users.list',
    action() {
        BlazeLayout.render('App_body', { main: 'User_profile_list'});
    },
});

FlowRouter.route('/product-list', {
    name: 'Product.list',
    action() {
        BlazeLayout.render('App_body', { main: 'Product_list'});
    },
});


FlowRouter.route('/product-show/:_id',{
  name: 'Product.show',
  action(){
    BlazeLayout.render('App_body', {main: 'Product_show'})
  }
})



FlowRouter.route('/categories-insert', {
    name: 'Categories.insert',
    action() {
        BlazeLayout.render('App_body', { main: 'Categories_insert'});
    },
});

AccountsTemplates.configureRoute('signIn', {
    name: 'signin',
    path: '/signin',
});

AccountsTemplates.configureRoute('signUp', {
    name: 'join',
    path: '/join',
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
    name: 'resetPwd',
    path: '/reset-password',
});
