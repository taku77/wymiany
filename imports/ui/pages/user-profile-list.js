import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';

import './user-profile-list.html';


Template.User_profile_list.helpers({
    users(){
        return Meteor.users.find({});
    }
})
