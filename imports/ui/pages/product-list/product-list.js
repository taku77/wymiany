import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';
import { ReactiveDict } from 'meteor/reactive-dict';
import {
    getAllProducts
} from '../../../api/products/methods.js';
import {
    ProductsWanted,
    ProductsGot
} from '../../../api/products/products.js';
import './product-list.html';

Template.Product_list.onCreated(function productListOnCreated(){
    this.state = new ReactiveDict();
    this.state.setDefault({
        editingProduct: false,
        searchBox: false
    });



    this.changeSearchBox = () => {

        this.state.set('searchBox', this.state.get('searchBox') ? false : true);
        ProductsGot.getComponentMethods().addProps('witchTags', this.state.get('searchBox'));
        Tracker.flush();

    }
});
Template.Product_list.onRendered(function productListOnRendered(){
   const instance =  Template.instance();
   $('.switcher').bootstrapSwitch({
       onText: 'Posiadam',
       offText: 'Szukam',
       onSwitchChange: function(event,state){
           instance.changeSearchBox();
       }
   });
});
Template.Product_list.helpers({
    searchAttr(){
        return {
            class: 'form-control'
        }
    },
    buttonAttr(){
      return{
          class: 'btn btn-primary search-button'

      }
    },

    productsIndex(){
        const instane = Template.instance();
        return ProductsGot ;
    },
    products(){
        return getAllProducts.call({
            userId: 'test'
        });
    },
     productArgs(product)
    {
        const instance = Template.instance();
        return {
            product,
            editing: instance.state.equals('editingTodo', product._id),
            onEditingChange(editing) {
                instance.state.set('editingProduct', editing ? todo._id : false);
            },
        }
    }
})

