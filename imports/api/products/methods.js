import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';


import { Products } from './products.js';

export const insert = new ValidatedMethod({
    name: 'products.insert',
    validate: new SimpleSchema({
        name: {type: String},
        description: {type: String},
        category: {type: String},
        productTags: {type: [String]},
        wantedCategory: {type: [String]},
        wantedTags: {type: [String]},
        status: {type: Boolean},


    }).validator(),
    run({name, description,category,wantedCategory,productTags,wantedTags,  status}){
        if (!this.userId) {
            throw new Meteor.Error('product.onlyForloged',
                'Dodawanie produktów tylko dla zalogowanych.');
        }

        const product ={
            name,
            description,
            wantedCategory,
            category,
            productTags,
            wantedTags,
            status,
            userId: this.userId,
            createdAt: new Date(),
        }

        return Products.insert(product);
    }

})


export const updateProduct = new ValidatedMethod({
    name: 'products.update',
    validate: new SimpleSchema({
        productId: {type: String},
        title: {type: String},
        description: {type: String},
    }).validator(),
    run({productId, title,desc}){
        if (!this.userId) {
            throw new Meteor.Error('products.editable.onlylogedin',
                'Edycja tylko dla zalogowanych.');
        }

        const product = Products.findOne(productId);
        if (!product.editableBy(this.userId)) {
            throw new Meteor.Error('products.notauthor',
                'Nie masz uprawnien do edycji tego produktu.');
        }

        Products.update(productId,{
            $set: {
                title: title,
                description: desc
            }
        });
    }
})
export const prodcutCounterForUser =  new ValidatedMethod({
    name: 'product.productcounterforuser',
    validate: new SimpleSchema({
        userId: {type: String}
    }).validator(),
    run({userId}){
        return Products.find({userId: userId}).count();
    }
})
export const getProductsForUser = new ValidatedMethod({
    name: 'product.getproductsforuser',
    validate: new SimpleSchema({
        userId: {type: String},
    }).validator(),
    run({userId}){
        return Products.find({userId: userId}, {sort: {createdAt: -1}});
    }
})
export const getAllProducts = new ValidatedMethod({
    name: 'product.getallproducts',
    validate: new SimpleSchema({
        userId: {type: String},
    }).validator(),
    run({userId}){
        return Products.find({},{sort: {createdAt: -1}});
    }
})

const PRODUCTS_METHODS = _.pluck([
    insert,
    updateProduct,
    getProductsForUser,
    prodcutCounterForUser,
    getAllProducts,

],'name');

if (Meteor.isServer) {
    // Only allow 5 todos operations per connection per second
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(PRODUCTS_METHODS, name);
        },

        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}
