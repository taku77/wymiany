import { Meteor } from 'meteor/meteor';
import { Categories } from '../categories.js';

Meteor.publish('categories.showall',function categoriesShowAll(){
    return Categories.find({},{fields: Categories.publicFields})
})
