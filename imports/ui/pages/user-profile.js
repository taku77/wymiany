import './user-profile.html';

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { ReactiveDict } from 'meteor/reactive-dict';
import {
    insert,
    getProductsForUser,
    prodcutCounterForUser
} from '../../api/products/methods.js';

import {
    getAllCategories
} from '../../api/categories/methods.js';
import { Images } from '../../api/images/images.js';

import { displayError } from '../lib/errors.js';

import { FlowRouter } from 'meteor/kadira:flow-router';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { TAPi18n } from 'meteor/tap:i18n';

Template.User_profile.onRendered(function userProfileOnRendered(){
    $('.tags').tagsinput({

    });
    $('.switcher').bootstrapSwitch({
        onText: 'Nowy',
        offText: 'Używany',

    });
})

Template.User_profile.onCreated(function userProfileOnCreated(){
    this.state = new ReactiveDict();
    this.state.setDefault({
        editingProduct: false,
    })
})


Template.User_profile.helpers({
    user(){
      return Meteor.users.findOne({_id: FlowRouter.getParam('_id')})
    },
    counter(){
      return prodcutCounterForUser.call({
          userId: FlowRouter.getParam('_id'),
      })
    },
    products(){
        return getProductsForUser.call({
            userId: FlowRouter.getParam('_id'),
        })
    },
    productArgs(product)
    {
        const instance = Template.instance();
        return {
            product,
            editing: instance.state.equals('editingTodo', product._id),
            onEditingChange(editing) {
                instance.state.set('editingProduct', editing ? todo._id : false);
            },
        }
    },
    categories(){
      return getAllCategories.call({parent: ''});
    },

})

Template.User_profile.events({
    'submit .js-product-new'(event){

        event.preventDefault();


        const $wantedCategoryIds = [];
        const $title = $(event.target).find('.title');
        const $desc = $(event.target).find('.desc');
        const $category = $(event.target).find('input[name="category"]:checked').val();
        const $wantedCategory = $(event.target).find('.categoryCheckbox:checked');
        const $productTags = $(event.target).find('.productTags');
        const $wantedTags = $(event.target).find('.wantedTags');
          const $status = $(event.target).find('.statusproduct').is(':checked');
        if(!$title.val() && !$desc.val())
            return;

            for (let i = 0, ln = $wantedCategory.length; i < ln; i++) {
              $wantedCategoryIds.push($wantedCategory[i].value);
            }
        const idProduct = insert.call({
                name : $title.val(),
                description: $desc.val(),
                productTags: $productTags.tagsinput('items'),
                wantedTags: $wantedTags.tagsinput('items'),
                category: $category,
                wantedCategory: $wantedCategoryIds,
                status: $status
            }, displayError);

        const files = $(event.target).find('.file-upload');
            for (let i = 0, ln = files.length; i < ln; i++) {
              if(typeof(files[i].files[0]) != 'undefined')
              {
                const fsFile = new FS.File(files[i].files[0]);
                fsFile.metadata = {
                    productId:idProduct
                    }

                  Images.insert(fsFile, function (err, fileObj) {
                      if(!err)
                      {

                      }
                      else {
                        console.log(err)
                      }
                  });
              }
            }



    }
})
Template.Category_checkbox.helpers({
  insideCats(){
    const idCat = this._id;
    return getAllCategories.call({parent: idCat});
  }
})

Template.Category_radio.helpers({
  insideCats(){
    const idCat = this._id;
    return getAllCategories.call({parent: idCat});
  }
})
