
import { Template } from 'meteor/templating';
import { Categories } from '../../api/categories/categories.js';

Template.registerHelper("categoryName", function(cat){
  const category = Categories.findOne(cat);
  if(typeof(category) != 'undefined')
    return category.name;
});

Template.registerHelper("statusCheck", function(state){
    if(state == true)
      return 'Nowy';
    else
      return 'Używany';
});
