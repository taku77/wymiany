import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { EasySearch } from 'meteor/easy:search';

export const Products = new Mongo.Collection('Products');

export const ProductsGot = new EasySearch.Index({
    collection: Products,
    fields: ['name','productTags','wantedTags'],
    name: 'productsGot',

    engine: new EasySearch.MongoDB({
        sort: () => {createdAt : -1},
        selector: function(searchObject, options, aggregation) {
            let selector = this.defaultConfiguration().selector(searchObject, options, aggregation);

            if(options.search.props.witchTags)
                selector['$or'].splice(2,1);
            else
                selector['$or'].splice(1,1);



            return selector;
        }
    }),
})
export const ProductsWanted = new EasySearch.Index({
    collection: Products,
    fields: ['name','wantedTags'],
    name: 'productsWanted',
    engine: new EasySearch.MongoDB(),
})

Products.deny({
        insert() { return true; },
        update() { return true; },
        remove() { return true; },
});


Products.schema = new SimpleSchema({
    name:{
        type: String,
        max: 100,
    },
    description:{
        type: String,
        max: 10000,
    },
    userId: {
        type: String,
    },
    category:{
      type: String,
      optional: true,
    },
    status:{
      type: Boolean,
      optional: true,
    },
    productTags: {
        type: [String],
        optional: true,
    },
    wantedCategory: {
        type: [String],
        optional: true,
    },
    wantedTags: {
        type: [String],
        optional: true,

    },
    images: {
        type: [String],
        optional: true,
    },
    createdAt: {
        type: Date,
        denyUpdate: true,
    },

});


Products.attachSchema(Products.schema);

Products.publicFields = {
    _id:1,
    name: 1,
    description: 1,
    createdAt: 1,
    userId: 1,
    productTags: 1,
    wantedTags: 1,
    images: 1,
    category:1,
    wantedCategory: 1,
    status: 1,

}


Products.helpers({
    editableBy(userId){
        if(!this.userId){
            return true;
        }

        return this.userId === userId;
    },

    returnAllByUserId(userID){
        return Products.find({userId: userID}, {sort: {createdAt: -1}});
    },
})
