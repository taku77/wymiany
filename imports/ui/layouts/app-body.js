import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ActiveRoute } from 'meteor/zimme:active-route';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { TAPi18n } from 'meteor/tap:i18n';


import './app-body.html';


Template.App_body.onCreated(function  appBodyOnCreated() {
    this.subscribe('products.all');
    this.subscribe('users.showall');
    this.subscribe('images.all');
  this.subscribe('categories.showall');
})



Template.App_body.events({
    'click .js-logout'(){
        Meteor.logout();
    }
})
