import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { ReactiveDict } from 'meteor/reactive-dict';
import {
    updateProduct
} from '../../api/products/methods.js';
import {
    findBackground
} from '../../api/images/methods.js';
import './product-box.html';


Template.Product_box.helpers({
    background(){
    
            return findBackground.call({
                ids : this.product._id.slice(0,17)
            });
    },
    editingClass(editing){
        return editing && 'editing'
    },
    productId(){
      return this.product._id.slice(0,17)
    }
})
