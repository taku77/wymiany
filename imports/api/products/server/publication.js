import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Products } from '../products.js';


Meteor.publish('products.all', function productsAll(){
    return Products.find({},{fields : Products.publicFields});
});