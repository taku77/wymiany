import './categories-insert.html';

import { Template } from 'meteor/templating';
import { Categories } from '../../../api/categories/categories.js';
import { _ } from 'meteor/underscore';


Template.Categories_insert.helpers({
  Categories(){
    return Categories;
  },
  options(){
    const optionsArray = [];
    const categories = Categories.find({})
    categories.forEach((obj)=>{
        optionsArray.push({'label' : obj.name, 'value' : obj._id})
    });

    return optionsArray;
  }
});
