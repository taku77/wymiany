import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { Categories } from './categories.js';


export const getAllCategories = new ValidatedMethod({
  name: 'categories.getallcategories',
  validate: new SimpleSchema({
      parent: {type: String},
  }).validator(),
  run({parent}){
    if(parent == '')
      parent = false;
    else {
      return Categories.find({parentId: parent})
    }

    return Categories.find({parentId: { $exists: parent }});
  }
})
